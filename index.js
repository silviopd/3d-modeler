const fileSelector = document.getElementById("file-selector");
fileSelector.addEventListener("change", (event) => {
  const fileList = event.target.files;
  console.log(fileList);

  readImage(fileList[0]);
});

function readImage(file) {
  // Check if the file is an image.
  if (file.type && file.type.indexOf("image") === -1) {
    console.log("File is not an image.", file.type, file);
    return;
  }

  const reader = new FileReader();
  reader.addEventListener("load", (event) => {
    const modelViewer = document.querySelector("model-viewer");
    modelViewer.src = event.target.result;
  });
  reader.readAsDataURL(file);
}

function toggleFullScreen() {
  var doc = window.document;
  var docEl = document.querySelector("#fullscreen");
  var requestFullScreen =
    docEl.requestFullscreen ||
    docEl.mozRequestFullScreen ||
    docEl.webkitRequestFullScreen ||
    docEl.msRequestFullscreen;
  var cancelFullScreen =
    doc.exitFullscreen ||
    doc.mozCancelFullScreen ||
    doc.webkitExitFullscreen ||
    doc.msExitFullscreen;

  if (
    !doc.fullscreenElement &&
    !doc.mozFullScreenElement &&
    !doc.webkitFullscreenElement &&
    !doc.msFullscreenElement
  ) {
    requestFullScreen.call(docEl);
  } else {
    cancelFullScreen.call(doc);
  }
}

document.getElementById("fullscreen-button").addEventListener("click", () => {
  toggleFullScreen();
});
